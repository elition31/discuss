// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: "discuss-322e5",
    appId: "1:1069536854318:web:e3590833c0583e54863abf",
    storageBucket: "discuss-322e5.appspot.com",
    apiKey: "AIzaSyDUjsorG2S0VaLmNmHeFuYwC_hG4uN96Tk",
    authDomain: "discuss-322e5.firebaseapp.com",
    messagingSenderId: "1069536854318",
    measurementId: "G-BJXE9H5J7F",
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
