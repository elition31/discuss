import { Component } from "@angular/core";
import { UsersService } from "./core/services/users.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  constructor(private usersService: UsersService) {}
}
