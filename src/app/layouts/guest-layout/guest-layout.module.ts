// Angular
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Components
import { GuestLayoutComponent } from "./guest-layout.component";

@NgModule({
  declarations: [GuestLayoutComponent],
  imports: [SharedModule],
  exports: [SharedModule, GuestLayoutComponent],
})
export class GuestLayoutModule {}
