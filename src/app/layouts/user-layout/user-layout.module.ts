// Angular
import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Components
import { UserLayoutComponent } from "./user-layout.component";

@NgModule({
  declarations: [UserLayoutComponent],
  imports: [SharedModule],
  exports: [SharedModule, UserLayoutComponent],
})
export class UserLayoutModule {}
