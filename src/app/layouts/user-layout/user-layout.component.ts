// Angular
import { Component, OnInit } from "@angular/core";

// Icons
import { faBars, faArrowLeftLong, IconDefinition, faUsers, faGear, faMessage, faThumbtack } from "@fortawesome/free-solid-svg-icons";
import { faCircleUser } from "@fortawesome/free-regular-svg-icons";

// Models
import { Discuss } from "src/app/core/models/discuss.interface";
import { Color } from "src/app/core/models/color.interface";

// Services
import { AuthenticationService } from "src/app/core/services/authentication.service";
import { ApplicationService } from "src/app/core/services/application.service";

@Component({
  selector: "app-user-layout",
  templateUrl: "./user-layout.component.html",
  styleUrls: ["./user-layout.component.scss"],
})
export class UserLayoutComponent implements OnInit {
  public returnPath: string = "";
  public color: Color | undefined = undefined;
  public currentDiscuss: Discuss | undefined = undefined;
  public isNavigationOpened: boolean = false;
  public currentTab: "messages" | "members" | "settings" | "pinnedMessages" = "messages";

  public burgerMenuIcon: IconDefinition = faBars;
  public returnIcon: IconDefinition = faArrowLeftLong;
  public dashboardIcon: IconDefinition = faMessage;
  public profileIcon: IconDefinition = faCircleUser;
  public messageIcon: IconDefinition = faMessage;
  public usersIcon: IconDefinition = faUsers;
  public settingsIcon: IconDefinition = faGear;
  public pinIcon: IconDefinition = faThumbtack;

  constructor(private applicationService: ApplicationService, private authService: AuthenticationService) {
    this.applicationService.returnPath.subscribe((value) => {
      this.returnPath = value;
    });
    this.applicationService.currentColor.subscribe((color) => {
      this.color = color;
    });
    this.applicationService.currentDiscuss.subscribe((discuss) => {
      this.currentDiscuss = discuss;
    });
    this.applicationService.discussSelectedTab.subscribe((tab) => {
      this.currentTab = tab;
    });
  }

  ngOnInit(): void {}

  public changeTab(tab: "messages" | "members" | "settings" | "pinnedMessages"): void {
    this.applicationService.discussSelectedTab.next(tab);
  }

  public isAllowed(): boolean {
    const authenticateUser = this.authService.getCurrentUser();
    if (authenticateUser) {
      return this.currentDiscuss!.owners.some((uid) => uid === authenticateUser.uid);
    } else {
      return false;
    }
  }
}
