// Angular
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Component, EventEmitter, Output } from "@angular/core";

// Utils
import { PASSWORD_REGEX } from "src/app/core/constants/regex.constants";

@Component({
  selector: "app-sign-in-form",
  templateUrl: "./sign-in-form.component.html",
  styleUrls: ["./sign-in-form.component.scss"],
})
export class SignInFormComponent {
  public form: FormGroup;

  @Output() submited: EventEmitter<{ email: string; password: string }> = new EventEmitter<{ email: string; password: string }>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.pattern(PASSWORD_REGEX)]),
    });
  }

  public async submit(): Promise<void> {
    this.submited.emit({ email: this.email?.value, password: this.password?.value });
  }

  get email(): AbstractControl | null {
    return this.form.get("email");
  }

  get password(): AbstractControl | null {
    return this.form.get("password");
  }
}
