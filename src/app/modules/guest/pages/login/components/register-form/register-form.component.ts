// Angular
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Component, EventEmitter, Output } from "@angular/core";

// Utils
import { PASSWORD_REGEX } from "src/app/core/constants/regex.constants";

@Component({
  selector: "app-register-form",
  templateUrl: "./register-form.component.html",
  styleUrls: ["./register-form.component.scss"],
})
export class RegisterFormComponent {
  public form: FormGroup;

  @Output() submited: EventEmitter<{ email: string; password: string; firstname: string; lastname: string }> = new EventEmitter<{
    email: string;
    password: string;
    firstname: string;
    lastname: string;
  }>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      firstname: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.pattern(PASSWORD_REGEX)]),
    });
  }

  public async submit(): Promise<void> {
    this.submited.emit({
      firstname: this.firstname?.value,
      lastname: this.lastname?.value,
      email: this.email?.value,
      password: this.password?.value,
    });
  }

  get firstname(): AbstractControl | null {
    return this.form.get("firstname");
  }

  get lastname(): AbstractControl | null {
    return this.form.get("lastname");
  }

  get email(): AbstractControl | null {
    return this.form.get("email");
  }

  get password(): AbstractControl | null {
    return this.form.get("password");
  }
}
