// Angular
import { Component } from "@angular/core";
import { Router } from "@angular/router";

// Constants
import { FIREBASE_MESSAGES } from "src/app/core/constants/firebase-messages.constants";

// Services
import { AuthenticationService } from "src/app/core/services/authentication.service";
import { ToastService } from "src/app/core/services/toast.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  public selectedTab: number = 1;

  constructor(private authService: AuthenticationService, private toastService: ToastService, private router: Router) {}

  public selectTab(tab: number): void {
    this.selectedTab = tab;
  }

  public async signIn(data: { email: string; password: string }): Promise<void> {
    try {
      await this.authService.signIn(data.email, data.password);
      this.toastService.addToast({ duration: 3000, message: "Bienvenu sur Discuss !", type: "success", button: "OK" });
      this.router.navigateByUrl("discuss/dashboard");
    } catch (error: any) {
      const message = FIREBASE_MESSAGES.find((m) => m.value === error.code)?.translation || "Une erreur a eu lieu !";
      this.toastService.addToast({ duration: 0, message: message, type: "error", button: "OK" });
    }
  }

  public async register(data: { email: string; password: string; firstname: string; lastname: string }): Promise<void> {
    try {
      await this.authService.signUp(data.email, data.password, data.firstname, data.lastname);
    } catch (error: any) {
      const message = FIREBASE_MESSAGES.find((m) => m.value === error.code)?.translation || "Une erreur a eu lieu !";
      this.toastService.addToast({ duration: 0, message, type: "error", button: "OK" });
    }
  }
}
