// Angular
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

// Guards
import { AngularFireAuthGuard, redirectLoggedInTo } from "@angular/fire/compat/auth-guard";

// Pages
import { LoginComponent } from "./pages/login/login.component";
import { LogoutComponent } from "./pages/logout/logout.component";

const redirectLoggedInToDashboard = () => redirectLoggedInTo("discuss/dashboard");

const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
  },
  {
    path: "",
    children: [
      {
        path: "login",
        component: LoginComponent,
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectLoggedInToDashboard },
      },
      {
        path: "logout",
        component: LogoutComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuestRoutingModule {}
