// Angular
import { NgModule } from "@angular/core";

// Routing
import { GuestRoutingModule } from "./guest-routing.module";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Pages
import { LoginComponent } from "./pages/login/login.component";
import { LogoutComponent } from "./pages/logout/logout.component";
import { SignInFormComponent } from "./pages/login/components/sign-in-form/sign-in-form.component";
import { RegisterFormComponent } from "./pages/login/components/register-form/register-form.component";

@NgModule({
  declarations: [LoginComponent, LogoutComponent, SignInFormComponent, RegisterFormComponent],
  imports: [SharedModule, GuestRoutingModule],
})
export class GuestModule {}
