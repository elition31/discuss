// Angular
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { Clipboard } from "@angular/cdk/clipboard";

// Models
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { Message } from "src/app/core/models/message.interface";
import { Color } from "src/app/core/models/color.interface";

// Icons
import { faEllipsisV, faReply } from "@fortawesome/free-solid-svg-icons";
import { MessageActionBottomSheetComponent } from "../../bottom-sheet/message-action-bottom-sheet/message-action-bottom-sheet.component";

@Component({
  selector: "app-message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"],
})
export class MessageComponent implements OnInit {
  @Input() message!: Message;
  @Input() userId!: string;
  @Input() color!: Color;

  @Output() replied: EventEmitter<Message> = new EventEmitter<Message>();
  @Output() actionEnabled: EventEmitter<{ type: "PIN" | "EDIT" | "DELETE" | "DEEP_DELETE"; message: Message }> = new EventEmitter<{
    type: "PIN" | "EDIT" | "DELETE" | "DEEP_DELETE";
    message: Message;
  }>();

  public optionIcon: IconDefinition = faEllipsisV;
  public replyIcon: IconDefinition = faReply;

  constructor(private bottomSheet: MatBottomSheet, private clipBoard: Clipboard) {}

  ngOnInit(): void {}

  public reply(message: Message): void {
    this.replied.emit(message);
  }

  public openOptions(): void {
    this.bottomSheet
      .open(MessageActionBottomSheetComponent, {
        data: { color: this.color, delete: this.message.isDeleted },
        panelClass: `bottom-sheet-${this.color.id}`,
      })
      .afterDismissed()
      .subscribe((response: "PIN" | "EDIT" | "DELETE" | "DEEP_DELETE" | "COPY" | undefined) => {
        if (response !== undefined) {
          switch (response) {
            case "COPY":
              this.copy();
              break;

            case "DELETE":
              this.delete(response);
              break;

            case "DEEP_DELETE":
              this.delete(response);
              break;

            case "EDIT":
              this.edit();
              break;

            case "PIN":
              this.pin();
              break;
          }
        }
      });
  }

  private copy(): void {
    this.clipBoard.copy(this.message.value);
  }

  private edit(): void {
    this.actionEnabled.emit({ type: "EDIT", message: this.message });
  }

  private pin(): void {
    this.actionEnabled.emit({ type: "PIN", message: this.message });
  }

  private delete(type: "DELETE" | "DEEP_DELETE"): void {
    this.actionEnabled.emit({ type: type, message: this.message });
  }
}
