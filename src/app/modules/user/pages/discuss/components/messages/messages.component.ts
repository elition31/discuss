// Angular
import { AfterViewChecked, AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from "@angular/core";

// Models
import { Message } from "src/app/core/models/message.interface";
import { Color } from "src/app/core/models/color.interface";
import { User } from "src/app/core/models/user.interface";

// Services
import { AuthenticationService } from "src/app/core/services/authentication.service";

// Firebase
import * as firebase from "firebase/firestore";
import { Timestamp } from "@firebase/firestore-types";

// Icons
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-messages",
  templateUrl: "./messages.component.html",
  styleUrls: ["./messages.component.scss"],
})
export class MessagesComponent implements AfterViewInit, AfterViewChecked, OnChanges {
  @ViewChild("messagesRef") private messagesRef!: ElementRef;

  @Input() messages: Message[] = [];
  @Input() color!: Color;

  @Output() messageSent: EventEmitter<Message> = new EventEmitter<Message>();
  @Output() messagePinned: EventEmitter<Message> = new EventEmitter<Message>();
  @Output() messageUpdated: EventEmitter<Message[]> = new EventEmitter<Message[]>();

  private editedMessage: Message | undefined = undefined;

  public message: string = "";
  public user: User;
  public replyMessage: Message | undefined = undefined;
  public isFocus: boolean = true;
  public isScrolling: boolean = false;
  public newMessages: boolean = false;

  public cancelReplyIcon: IconDefinition = faTimes;

  constructor(private authService: AuthenticationService) {
    this.user = this.authService.getCurrentUser()!;
  }

  onScroll(event: any): void {
    this.isScrolling = event.target.scrollHeight - event.target.offsetHeight !== event.target.scrollTop;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["messages"] && changes["messages"].previousValue && this.isScrolling) {
      this.newMessages = changes["messages"].previousValue.length < changes["messages"].currentValue.length;
    }
  }

  ngAfterViewChecked(): void {
    if (!this.isScrolling) {
      this.scrollBottom(false);
    }
  }

  ngAfterViewInit(): void {
    this.scrollBottom(false);
  }

  public displayNewMessages(): void {
    this.isScrolling = false;
    this.newMessages = false;
    this.scrollBottom(false);
  }

  public sendMessage(): void {
    let responseFrom: { message: string; ownerId: string; createdAt: Timestamp } | null = null;

    if (this.replyMessage) {
      responseFrom = {
        message: this.replyMessage.value,
        ownerId: this.replyMessage.ownerId,
        createdAt: this.replyMessage.createdAt,
      };
    }

    if (this.editedMessage && this.editedMessage.value !== "") {
      this.editedMessage.responseFrom = responseFrom;
      this.editedMessage.updatedAt = firebase.Timestamp.now();
      this.editedMessage.value = this.message;

      let message = this.messages.find((m) => m.createdAt === this.editedMessage?.createdAt && m.ownerId === this.editedMessage.ownerId);
      message = this.editedMessage;

      this.messageUpdated.emit(this.messages);
      this.message = "";
      this.replyMessage = undefined;
    } else if (this.message !== "") {
      const date = firebase.Timestamp.now();
      const message: Message = {
        ownerId: this.user.uid,
        createdAt: date,
        updatedAt: date,
        isDeleted: false,
        likeAmount: 0,
        responseFrom,
        value: this.message,
      };

      this.messageSent.emit(message);
      this.message = "";
      this.replyMessage = undefined;
      this.scrollBottom();
    }
  }

  public reply(message: Message): void {
    this.replyMessage = message;
    this.isFocus = true;
  }

  public cancelReply(): void {
    this.replyMessage = undefined;
  }

  public editLastMessage(): void {
    // HACK - Copy without reference
    const messages: Message[] = this.messages.filter((m) => m.ownerId);
    const message: Message | undefined = messages.reverse().find((message: any) => message.ownerId === this.user.uid);

    if (message) {
      this.editedMessage = message;
      this.message = message.value;

      if (message.responseFrom !== null) {
        this.replyMessage = {
          ownerId: message.responseFrom.ownerId,
          createdAt: firebase.Timestamp.now(),
          updatedAt: firebase.Timestamp.now(),
          isDeleted: false,
          likeAmount: 0,
          responseFrom: null,
          value: message.responseFrom.message,
        };
      }
    }
  }

  public actionCalled(action: { type: "PIN" | "EDIT" | "DELETE" | "DEEP_DELETE"; message: Message }): void {
    switch (action.type) {
      case "DELETE":
        this.delete(action.message, action.type);
        break;

      case "DEEP_DELETE":
        this.delete(action.message, action.type);
        break;

      case "EDIT":
        this.edit(action.message);
        break;

      case "PIN":
        this.messagePinned.emit(action.message);
        break;
    }
  }

  public scrollBottom(smooth: boolean = false): void {
    try {
      this.messagesRef.nativeElement.scrollTo({ left: 0, top: this.messagesRef.nativeElement.scrollHeight, behavior: smooth ? "smooth" : "auto" });
    } catch (error) {
      console.log(error);
    }
  }

  private edit(message: Message): void {
    this.editedMessage = message;
    this.message = message.value;

    if (message.responseFrom !== null) {
      this.replyMessage = {
        ownerId: message.responseFrom.ownerId,
        createdAt: firebase.Timestamp.now(),
        updatedAt: firebase.Timestamp.now(),
        isDeleted: false,
        likeAmount: 0,
        responseFrom: null,
        value: message.responseFrom.message,
      };
    }
  }

  private delete(deletedMessage: Message, type: "DELETE" | "DEEP_DELETE"): void {
    if (type === "DEEP_DELETE") {
      this.messages.splice(
        this.messages.findIndex((m) => m.ownerId === deletedMessage.ownerId && m.createdAt.isEqual(deletedMessage.createdAt)),
        1
      );
    } else {
      const message: Message = this.messages.find((m) => m.ownerId === deletedMessage.ownerId && m.createdAt.isEqual(deletedMessage.createdAt))!;
      message.isDeleted = true;
      message.updatedAt = firebase.Timestamp.now();
    }

    const reply = this.messages.find((m) => m.responseFrom?.ownerId === deletedMessage.ownerId && m.responseFrom.createdAt.isEqual(deletedMessage.createdAt));

    if (reply !== undefined && reply.responseFrom !== null) {
      reply.responseFrom.message = "Message supprimé";
    }

    this.messageUpdated.emit(this.messages);
  }
}
