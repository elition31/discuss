// Angular
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { Component } from "@angular/core";

// Models
import { Discuss } from "src/app/core/models/discuss.interface";
import { Message } from "src/app/core/models/message.interface";
import { Chanel } from "src/app/core/models/chanel.interface";
import { Color } from "src/app/core/models/color.interface";

// Services
import { ApplicationService } from "src/app/core/services/application.service";
import { AnalyticsService } from "src/app/core/services/analytics.service";
import { DiscussService } from "src/app/core/services/discuss.service";
import { LoaderService } from "src/app/core/services/loader.service";

// Enums
import { COLORS } from "src/app/core/constants/colors.constants";

@Component({
  selector: "app-discuss",
  templateUrl: "./discuss.component.html",
  styleUrls: ["./discuss.component.scss"],
})
export class DiscussComponent {
  public discuss: Discuss | undefined = undefined;
  public color: Color = COLORS[0];
  public chanel: Chanel | undefined = undefined;
  public currentTab: "messages" | "members" | "settings" | "pinnedMessages" = "messages";
  public messages: Message[] = [];

  constructor(
    private applicationService: ApplicationService,
    private analyticsService: AnalyticsService,
    private discussService: DiscussService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router
  ) {
    const loaderId = this.loaderService.addLoader({ state: true, isMessageDisplayed: true, message: "Chargement du Discuss" });

    this.applicationService.currentColor.subscribe((currentColor) => {
      if (currentColor) {
        this.color = currentColor;
      }
    });

    this.route.paramMap.subscribe((params) => {
      const discussUid = params.get("uid");

      if (discussUid) {
        this.analyticsService.add("discuss_opened", { uid: discussUid });
        const currentDiscuss: Discuss | undefined = this.applicationService.currentDiscuss.getValue();

        if (currentDiscuss) {
          this.discuss = currentDiscuss;
          this.applicationService.currentColor.next(COLORS.find((c) => c.id === this.discuss!.colorId));
          this.loaderService.deleteLoaderById(loaderId);
          this.initMessagesListener();
        } else {
          this.discussService.fetchDiscussByUid(discussUid).subscribe((discuss) => {
            this.discuss = discuss;
            this.loaderService.deleteLoaderById(loaderId);

            if (this.discuss) {
              this.applicationService.returnPath.next("/discuss/dashboard");
              this.applicationService.currentDiscuss.next(discuss);
              this.initMessagesListener();

              if (this.applicationService.currentColor.value === undefined) {
                this.applicationService.currentColor.next(COLORS.find((c) => c.id === this.discuss!.colorId));
              }
            } else {
              this.router.navigateByUrl("/discuss/dashboard");
            }
          });
        }

        this.applicationService.discussSelectedTab.subscribe((tab) => {
          this.currentTab = tab;
        });
      } else {
        this.loaderService.deleteLoaderById(loaderId);
      }
    });
  }

  public async sendMessage(message: Message): Promise<void> {
    if (this.chanel) {
      try {
        this.chanel.messages.push(message);
        await this.discussService.addMessageToChanel(this.chanel.uid, this.chanel.messages);
      } catch (error) {
        console.log(error);
      }
    }
  }

  public async updateMessage(messages: Message[]): Promise<void> {
    if (this.chanel) {
      try {
        this.chanel.messages = messages;
        await this.discussService.addMessageToChanel(this.chanel.uid, this.chanel.messages);
      } catch (error) {
        console.log(error);
      }
    }
  }

  public async pinMessage(message: Message): Promise<void> {
    if (this.chanel && this.chanel.pinMessages.find((m) => m.createdAt === message.createdAt) === undefined) {
      this.chanel.pinMessages.push(message);

      try {
        this.discussService.pinMessage(this.chanel.uid, this.chanel.pinMessages);
      } catch (error) {
        console.log(error);
      }
    }
  }

  private initMessagesListener(): void {
    this.discussService.fetchChanelByUid(this.discuss!.chanelId).subscribe((chanel) => {
      this.chanel = chanel;
      if (this.chanel) {
        this.messages = this.chanel.messages;
      }
    });
  }
}
