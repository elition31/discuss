// Angular
import { Component, Inject } from "@angular/core";
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from "@angular/material/bottom-sheet";

// Icons
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faCopy, faEdit, faThumbtack, faTrash } from "@fortawesome/free-solid-svg-icons";

// Models
import { Color } from "src/app/core/models/color.interface";

@Component({
  selector: "app-message-action-bottom-sheet",
  templateUrl: "./message-action-bottom-sheet.component.html",
  styleUrls: ["./message-action-bottom-sheet.component.scss"],
})
export class MessageActionBottomSheetComponent {
  public copyIcon: IconDefinition = faCopy;
  public editIcon: IconDefinition = faEdit;
  public trashIcon: IconDefinition = faTrash;
  public pinIcon: IconDefinition = faThumbtack;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: { color: Color; delete: boolean },
    private bottomSheetRef: MatBottomSheetRef<MessageActionBottomSheetComponent>
  ) {}

  public clicked(action: "PIN" | "EDIT" | "COPY" | "DELETE" | "DEEP_DELETE"): void {
    this.bottomSheetRef.dismiss(action);
  }
}
