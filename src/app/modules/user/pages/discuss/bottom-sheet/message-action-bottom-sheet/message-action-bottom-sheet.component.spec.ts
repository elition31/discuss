import { ComponentFixture, TestBed } from "@angular/core/testing";

import { MessageActionBottomSheetComponent } from "./message-action-bottom-sheet.component";

describe("MessageActionBottomSheetComponent", () => {
  let component: MessageActionBottomSheetComponent;
  let fixture: ComponentFixture<MessageActionBottomSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessageActionBottomSheetComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageActionBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
