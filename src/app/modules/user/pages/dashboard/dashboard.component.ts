// Angular
import { MatDialog } from "@angular/material/dialog";
import { Component } from "@angular/core";
import { Router } from "@angular/router";

// Models
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { Discuss } from "src/app/core/models/discuss.interface";
import { User } from "src/app/core/models/user.interface";

// Services
import { AuthenticationService } from "src/app/core/services/authentication.service";
import { ApplicationService } from "src/app/core/services/application.service";
import { AnalyticsService } from "src/app/core/services/analytics.service";
import { DiscussService } from "src/app/core/services/discuss.service";

// Icons
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons";

// Modals
import { ConfirmationModalComponent } from "src/app/shared/modals/confirmation-modal/confirmation-modal.component";
import { CreateDiscussModalComponent } from "../../modals/create-discuss-modal/create-discuss-modal.component";

// Enums
import { COLORS } from "src/app/core/constants/colors.constants";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent {
  public plusIcon: IconDefinition = faCirclePlus;
  public searchValue: string = "";
  public discuss: Discuss[] = [];
  public myDiscuss: Discuss[] = [];
  public animationindex: number = -1;

  private rawDiscuss: Discuss[] = [];
  private rawMyDiscuss: Discuss[] = [];

  constructor(
    private applicationService: ApplicationService,
    private authService: AuthenticationService,
    private analyticsService: AnalyticsService,
    private discussService: DiscussService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.applicationService.currentColor.next(undefined);
    this.applicationService.returnPath.next("");
    this.applicationService.currentDiscuss.next(undefined);

    this.discussService.discuss$.subscribe((data) => {
      this.rawDiscuss = data.filter((d) => this.myDiscuss.find((m) => m.uid === d.uid) === undefined);
      this.discuss = this.rawDiscuss;
    });

    this.discussService.myDiscuss$.subscribe((data) => {
      this.rawMyDiscuss = data;
      this.myDiscuss = this.rawMyDiscuss;
      this.rawDiscuss = this.rawDiscuss.filter((d) => this.myDiscuss.find((m) => m.uid === d.uid) === undefined);
      this.discuss = this.rawDiscuss;

      if (this.myDiscuss.length > 0 && this.animationindex === -1) {
        this.animationindex = 0;
        this.initAnimation();
      }
    });
  }

  public createNewDiscuss(): void {
    this.dialog
      .open(CreateDiscussModalComponent, { disableClose: true, panelClass: "primary-modal" })
      .afterClosed()
      .subscribe((discuss: Discuss | undefined) => {
        if (discuss) {
          this.submit(discuss);
        }
      });
  }

  public search(): void {
    if (this.searchValue === "") {
      this.discuss = this.rawDiscuss;
      this.myDiscuss = this.rawMyDiscuss;
    } else {
      this.discuss = this.rawDiscuss.filter(
        (discuss) =>
          discuss.designation.toLocaleLowerCase().includes(this.searchValue.toLocaleLowerCase()) ||
          discuss.tags.some((tag) => tag.toLocaleLowerCase().includes(this.searchValue.toLocaleLowerCase()))
      );
      this.myDiscuss = this.rawMyDiscuss.filter(
        (discuss) =>
          discuss.designation.toLocaleLowerCase().includes(this.searchValue.toLocaleLowerCase()) ||
          discuss.tags.some((tag) => tag.toLocaleLowerCase().includes(this.searchValue.toLocaleLowerCase()))
      );
    }
  }

  public openDiscuss(discuss: Discuss): void {
    this.applicationService.currentColor.next(COLORS.find((c) => c.id === discuss!.colorId));
    this.applicationService.currentDiscuss.next(discuss);
    this.applicationService.returnPath.next("discuss/dashboard");
    this.router.navigateByUrl(`/discuss/chanel/${discuss.uid}`);
  }

  public joinDiscuss(discuss: Discuss): void {
    this.dialog
      .open(ConfirmationModalComponent, {
        data: {
          title: `Vous allez rejoindre le Discuss : ${discuss.designation}`,
          description: "Pour rejoindre ce Discuss, vous devez cliquer sur le bouton confirmer.",
        },
        disableClose: true,
        panelClass: "primary-modal",
      })
      .afterClosed()
      .subscribe(async (response: "cancel" | "confirm") => {
        if (response === "confirm") {
          const user: User | undefined = this.authService.getCurrentUser();
          discuss.members.push(user!.uid);

          try {
            await this.discussService.updateDiscuss(discuss.uid, discuss.members);
            this.router.navigateByUrl(`/discuss/chanel/${discuss.uid}`);
          } catch (error) {
            console.log(error);
            // AFFICHER LE TOAST
          }
        }
      });
  }

  private async submit(discuss: Discuss): Promise<void> {
    try {
      const uid = await this.discussService.createDiscuss(discuss);
      this.analyticsService.add("create_discuss", discuss.designation);
      this.router.navigateByUrl(`discuss/chanel/${uid}`);
    } catch (error) {
      console.log(error);
    }
  }

  private initAnimation(): void {
    const id = setInterval(() => {
      this.animationindex++;
      if (this.animationindex === this.myDiscuss.length) {
        clearInterval(id);
      }
    }, 200);
  }
}
