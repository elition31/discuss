// Angular
import { Component, EventEmitter, Input, Output } from "@angular/core";

// Models
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { Discuss } from "src/app/core/models/discuss.interface";

// Icons
import { faUsers, faCalendarDays } from "@fortawesome/free-solid-svg-icons";
import { ColorPipe } from "src/app/shared/pipes/color.pipe";

@Component({
  selector: "app-discuss-card",
  templateUrl: "./discuss-card.component.html",
  styleUrls: ["./discuss-card.component.scss"],
})
export class DiscussCardComponent {
  @Input() discuss!: Discuss;

  @Output() discussOpened: EventEmitter<Discuss> = new EventEmitter<Discuss>();

  public hovered: boolean = false;
  public usersIcon: IconDefinition = faUsers;
  public calendarIcon: IconDefinition = faCalendarDays;

  constructor(private colorPipe: ColorPipe) {}

  public openDiscuss(): void {
    this.discussOpened.emit(this.discuss);
  }

  public getColor(property: string): string {
    return this.colorPipe.transform(this.discuss.colorId, property);
  }
}
