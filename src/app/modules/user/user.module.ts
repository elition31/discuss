// Angular
import { NgModule } from "@angular/core";

// Routing
import { UserRoutingModule } from "./user-routing.module";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Pages
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { DiscussComponent } from "./pages/discuss/discuss.component";
import { ProfileComponent } from "./pages/profile/profile.component";

// Modals
import { CreateDiscussModalComponent } from "./modals/create-discuss-modal/create-discuss-modal.component";

// Bottom Sheets
import { MessageActionBottomSheetComponent } from "./pages/discuss/bottom-sheet/message-action-bottom-sheet/message-action-bottom-sheet.component";

// Components
import { PinnedMessagesComponent } from "./pages/discuss/components/pinned-messages/pinned-messages.component";
import { DiscussCardComponent } from "./pages/dashboard/components/discuss-card/discuss-card.component";
import { MessagesComponent } from "./pages/discuss/components/messages/messages.component";
import { SettingsComponent } from "./pages/discuss/components/settings/settings.component";
import { MembersComponent } from "./pages/discuss/components/members/members.component";
import { MessageComponent } from "./pages/discuss/components/message/message.component";

const PAGES = [DashboardComponent, DiscussComponent, ProfileComponent];

const MODALS = [CreateDiscussModalComponent];

const BOTTOM_SHEETS = [MessageActionBottomSheetComponent];

const COMPONENTS = [DiscussCardComponent, MessagesComponent, MembersComponent, SettingsComponent, MessageComponent, PinnedMessagesComponent];

@NgModule({
  declarations: [...PAGES, ...MODALS, ...BOTTOM_SHEETS, ...COMPONENTS],
  imports: [SharedModule, UserRoutingModule],
})
export class UserModule {}
