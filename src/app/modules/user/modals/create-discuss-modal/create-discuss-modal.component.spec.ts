import { ComponentFixture, TestBed } from "@angular/core/testing";

import { CreateDiscussModalComponent } from "./create-discuss-modal.component";

describe("CreateDiscussModalComponent", () => {
  let component: CreateDiscussModalComponent;
  let fixture: ComponentFixture<CreateDiscussModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateDiscussModalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDiscussModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
