// Angular
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { Component } from "@angular/core";

// Libraries
import { v4 as uuidv4 } from "uuid";
import * as firebase from "firebase/firestore";

// Models
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { Discuss } from "src/app/core/models/discuss.interface";
import { Color } from "src/app/core/models/color.interface";

// Constants
import { COLORS } from "src/app/core/constants/colors.constants";

// Icons
import { faCheck, faLock, faLockOpen } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-create-discuss-modal",
  templateUrl: "./create-discuss-modal.component.html",
  styleUrls: ["./create-discuss-modal.component.scss"],
})
export class CreateDiscussModalComponent {
  public form: FormGroup;
  public tags: string[] = [];
  public selectedColor: Color = COLORS[0];
  public colors: Color[] = COLORS;
  public isPrivate: boolean = false;
  public checkIcon: IconDefinition = faCheck;
  public lockIcon: IconDefinition = faLock;
  public lockOpenIcon: IconDefinition = faLockOpen;

  constructor(public dialogRef: MatDialogRef<CreateDiscussModalComponent>, private fb: FormBuilder) {
    this.form = this.fb.group({
      designation: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
    });
  }

  get designation(): AbstractControl | null {
    return this.form.get("designation");
  }

  get description(): AbstractControl | null {
    return this.form.get("description");
  }

  public selectColor(color: Color): void {
    this.selectedColor = color;
  }

  public togglePrivate(): void {
    this.isPrivate = !this.isPrivate;
  }

  public submit(): void {
    const date = firebase.Timestamp.now();
    const discuss: Discuss = {
      uid: "",
      designation: this.designation?.value,
      description: this.description?.value,
      createdAt: date,
      members: [],
      owners: [],
      tags: this.tags,
      chanelId: "",
      password: this.isPrivate ? uuidv4() : "",
      colorId: this.selectedColor.id,
    };
    this.dialogRef.close(discuss);
  }

  public cancel(): void {
    this.dialogRef.close();
  }
}
