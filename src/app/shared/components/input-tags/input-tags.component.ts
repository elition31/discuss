import { Component, EventEmitter, Input, Output } from "@angular/core";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-input-tags",
  templateUrl: "./input-tags.component.html",
  styleUrls: ["./input-tags.component.scss"],
})
export class InputTagsComponent {
  @Input() tags: string[] = [];
  @Input() limit: number = 5;

  @Output() tagsChange: EventEmitter<string[]> = new EventEmitter<string[]>();

  public tag: string = "";
  public deleteIcon: IconDefinition = faTimes;

  constructor() {}

  public addTag(): void {
    if (this.tag !== "") {
      this.tags.push(this.tag);
      this.tagsChange.emit(this.tags);
      this.tag = "";
    }
  }

  public removeTag(index: number): void {
    this.tags.splice(index, 1);
    this.tagsChange.emit(this.tags);
  }
}
