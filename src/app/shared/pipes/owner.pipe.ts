import { Pipe, PipeTransform } from "@angular/core";
import { User } from "src/app/core/models/user.interface";
import { UsersService } from "src/app/core/services/users.service";

@Pipe({
  name: "owner",
})
export class OwnerPipe implements PipeTransform {
  constructor(private usersService: UsersService) {}

  /**
   * This pipe take an UID and return a user from Firestore Database
   * @param {string} uid
   * @returns {User} a user
   */
  transform(uid: string): string {
    const user: User = this.usersService.getUsers().find((user) => user.uid === uid)!;
    return `${user.firstname} ${user.lastname.charAt(0).toUpperCase()}.`;
  }
}
