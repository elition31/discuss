import { Pipe, PipeTransform } from "@angular/core";
import { COLORS } from "src/app/core/constants/colors.constants";
import { Color } from "src/app/core/models/color.interface";

@Pipe({
  name: "color",
})
export class ColorPipe implements PipeTransform {
  transform(id: number, property: string): string {
    const color: Color = COLORS.find((c) => c.id === id)!;

    switch (property) {
      case "primary":
        return color.primary;
      case "secondary":
        return color.secondary;
      case "shadow":
        return color.shadow;
      case "message-owner":
        return color.message.owner;
      case "message-other":
        return color.message.other;
      case "message-reply":
        return color.message.reply;
      default:
        return color.primary;
    }
  }
}
