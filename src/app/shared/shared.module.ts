// Angular
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

// Angular Material Module
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatRadioModule } from "@angular/material/radio";
import { MatChipsModule } from "@angular/material/chips";
import { ClipboardModule } from "@angular/cdk/clipboard";
import { MatTabsModule } from "@angular/material/tabs";
import { MatMenuModule } from "@angular/material/menu";

// Modules
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

// Modals
import { ConfirmationModalComponent } from "./modals/confirmation-modal/confirmation-modal.component";

// Components
import { InputTagsComponent } from "./components/input-tags/input-tags.component";
import { ToastComponent } from "./components/toasts/toast/toast.component";
import { LoadersComponent } from "./components/loaders/loaders.component";
import { SpinnerComponent } from "./components/spinner/spinner.component";
import { ToastsComponent } from "./components/toasts/toasts.component";
import { TagsComponent } from "./components/tags/tags.component";

// Pipes
import { OwnerPipe } from "./pipes/owner.pipe";
import { ColorPipe } from "./pipes/color.pipe";

// Directives
import { AutofocusDirective } from "./directives/auto-focus.directive";

const MATERIAL_MODULE = [
  MatProgressSpinnerModule,
  MatBottomSheetModule,
  MatSlideToggleModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatSidenavModule,
  MatTooltipModule,
  MatDialogModule,
  MatSelectModule,
  ClipboardModule,
  MatRadioModule,
  MatInputModule,
  MatChipsModule,
  MatTabsModule,
  MatMenuModule,
];

const MODULES = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, FontAwesomeModule, ...MATERIAL_MODULE];

const PIPES = [OwnerPipe, ColorPipe];

const COMPONENTS = [...PIPES, LoadersComponent, SpinnerComponent, ToastsComponent, ToastComponent, InputTagsComponent, TagsComponent];

const DIRECTIVES = [AutofocusDirective];

const MODALS = [ConfirmationModalComponent];

const BOTTOM_SHEETS: never[] = [];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES, ...MODALS, ...BOTTOM_SHEETS],
  imports: [...MODULES],
  exports: [...MODULES, ...COMPONENTS, ...DIRECTIVES, ...MODALS, ...BOTTOM_SHEETS],
  providers: [...PIPES],
})
export class SharedModule {}
