// Angular
import { NgModule } from "@angular/core";

// Routing
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// Firebase
import { ScreenTrackingService, UserTrackingService } from "@angular/fire/analytics";
import { AngularFireAuthGuardModule } from "@angular/fire/compat/auth-guard";
import { AngularFireAnalyticsModule } from "@angular/fire/compat/analytics";
import { AngularFireDatabaseModule } from "@angular/fire/compat/database";
import { AngularFireStorageModule } from "@angular/fire/compat/storage";
import { AngularFirestoreModule } from "@angular/fire/compat/firestore";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AuthGuardModule } from "@angular/fire/auth-guard";
import { AngularFireModule } from "@angular/fire/compat";

// Environment
import { environment } from "../environments/environment";

// Modules
import { LayoutsModule } from "./layouts/layouts.module";
import { SharedModule } from "./shared/shared.module";
import { CoreModule } from "./core/core.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    SharedModule,
    LayoutsModule,
    AuthGuardModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthGuardModule,
    AngularFireAnalyticsModule,
  ],
  providers: [ScreenTrackingService, UserTrackingService],
  bootstrap: [AppComponent],
})
export class AppModule {}
