import { Timestamp } from "@firebase/firestore-types";

export interface User {
  uid: string;
  firstname: string;
  lastname: string;
  createdAt: Timestamp;
}
