import { Message } from "./message.interface";

export interface Chanel {
  uid: string;
  messages: Message[];
  pinMessages: Message[];
}
