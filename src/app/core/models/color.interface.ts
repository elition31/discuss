export interface Color {
  id: number;
  primary: string;
  secondary: string;
  shadow: string;
  message: {
    owner: string;
    other: string;
    reply: string;
  };
}
