import { Timestamp } from "@firebase/firestore-types";

export interface Message {
  ownerId: string;
  value: string;
  responseFrom: {
    message: string;
    ownerId: string;
    createdAt: Timestamp;
  } | null;
  likeAmount: number;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  isDeleted: boolean;
}
