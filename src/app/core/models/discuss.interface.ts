import { Timestamp } from "@firebase/firestore-types";

export interface Discuss {
  uid: string;
  designation: string;
  description: string;
  colorId: number;
  owners: string[];
  password: string;
  members: string[];
  tags: string[];
  chanelId: string;
  createdAt: Timestamp;
}
