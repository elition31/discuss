// Angular
import { Injectable } from "@angular/core";

// Firebase
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/compat/firestore";

// Models
import { User } from "../models/user.interface";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  /**
   * The list that contains all the users of the app
   */
  private users: User[] = [];

  /**
   * A reference to a Firestore Collection for the users
   */
  private usersCollection: AngularFirestoreCollection<User>;

  constructor(private afs: AngularFirestore) {
    this.usersCollection = this.afs.collection<User>("users");
    this.fetchUsersFromFirestore();
  }

  /**
   * This method is a simple getter of users
   * @returns an array of users
   */
  public getUsers(): User[] {
    return this.users;
  }

  public getUserById(uid: string) {
    return this.usersCollection.doc(uid).get();
  }

  /**
   * This method allows a user to be created in the Firestore Database
   * @param {User} user
   */
  public addUser(user: User): Promise<void> {
    return this.usersCollection.doc(user.uid).set(user);
  }

  /**
   * This method listen and fetch the users from the Firestore Database
   */
  private fetchUsersFromFirestore(): void {
    this.usersCollection.valueChanges().subscribe((users) => {
      this.users = users;
    });
  }
}
