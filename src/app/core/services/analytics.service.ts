import { Injectable } from "@angular/core";
import { AngularFireAnalytics } from "@angular/fire/compat/analytics";

@Injectable({
  providedIn: "root",
})
export class AnalyticsService {
  constructor(private analyticsFire: AngularFireAnalytics) {}

  public add(event: string, value: any) {
    this.analyticsFire.logEvent(event, value);
  }
}
