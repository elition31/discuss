// Angular
import { Injectable } from "@angular/core";

// Firebase
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/compat/firestore";

// Libraries
import { BehaviorSubject, Observable } from "rxjs";

// Models
import { Discuss } from "../models/discuss.interface";
import { Message } from "../models/message.interface";
import { Chanel } from "../models/chanel.interface";

// Services
import { AuthenticationService } from "./authentication.service";
import { LoaderService } from "./loader.service";

@Injectable({
  providedIn: "root",
})
export class DiscussService {
  /**
   * A reference to a Firestore Collection for the discuss
   */
  private discussCollection: AngularFirestoreCollection<Discuss>;
  private myDiscussCollection: AngularFirestoreCollection<Discuss> | undefined;

  public discuss$: BehaviorSubject<Discuss[]> = new BehaviorSubject<Discuss[]>([]);
  public myDiscuss$: BehaviorSubject<Discuss[]> = new BehaviorSubject<Discuss[]>([]);

  constructor(private afs: AngularFirestore, private authService: AuthenticationService, private loaderService: LoaderService) {
    this.discussCollection = this.afs.collection<Discuss>("discuss", (ref) => ref.where("password", "==", ""));

    const id = setInterval(() => {
      if (this.authService.isAuthReady) {
        this.myDiscussCollection = this.afs.collection<Discuss>("discuss", (ref) =>
          ref.where("members", "array-contains", this.authService.getCurrentUser()?.uid)
        );
        this.fetchMyDiscussFromFirestore();
        clearInterval(id);
      }
    }, 500);
    this.fetchDiscussFromFirestore();
  }

  public fetchDiscussByPassword(password: string): Observable<firebase.default.firestore.QuerySnapshot<Discuss>> {
    const collection: AngularFirestoreCollection<Discuss> = this.afs.collection<Discuss>("discuss", (ref) => ref.where("password", "==", password));
    return collection.get();
  }

  public fetchDiscussByUid(uid: string): Observable<Discuss | undefined> {
    return this.afs.doc<Discuss | undefined>(`discuss/${uid}`).valueChanges();
  }

  public updateDiscuss(uid: string, members: string[]): Promise<void> {
    return this.afs.doc<Discuss | undefined>(`discuss/${uid}`).update({ members });
  }

  public fetchChanelByUid(uid: string): Observable<Chanel | undefined> {
    return this.afs.doc<Chanel | undefined>(`chanel/${uid}`).valueChanges();
  }

  public addMessageToChanel(uid: string, messages: Message[]): Promise<void> {
    return this.afs.doc<Chanel | undefined>(`chanel/${uid}`).update({ messages });
  }

  public pinMessage(uid: string, pinMessages: Message[]): Promise<void> {
    return this.afs.doc<Chanel | undefined>(`chanel/${uid}`).update({ pinMessages });
  }

  public createDiscuss(discuss: Discuss): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const chanel: Chanel = { uid: this.afs.createId(), messages: [], pinMessages: [] };
        await this.afs.collection<Chanel>("chanel").doc(chanel.uid).set(chanel);

        discuss.uid = this.afs.createId();
        discuss.chanelId = chanel.uid;
        discuss.owners.push(this.authService.getCurrentUser()?.uid!);
        discuss.members.push(this.authService.getCurrentUser()?.uid!);

        await this.afs.collection<Discuss>("discuss").doc(discuss.uid).set(discuss);

        resolve(discuss.uid);
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * This method listen and fetch the public discuss from the Firestore Database
   */
  private fetchDiscussFromFirestore(): void {
    const loaderId = this.loaderService.addLoader({ state: true, isMessageDisplayed: true, message: "Chargement" });
    this.discussCollection.valueChanges().subscribe((discuss) => {
      this.discuss$.next(discuss);
      setTimeout(() => {
        this.loaderService.deleteLoaderById(loaderId);
      }, 500);
    });
  }

  /**
   * This method listen and fetch the discuss that the user is a member from the Firestore Database
   */
  private fetchMyDiscussFromFirestore(): void {
    const loaderId = this.loaderService.addLoader({ state: true, isMessageDisplayed: true, message: "Chargement" });
    this.myDiscussCollection!.valueChanges().subscribe((discuss) => {
      this.myDiscuss$.next(discuss);
      setTimeout(() => {
        this.loaderService.deleteLoaderById(loaderId);
      }, 500);
    });
  }
}
