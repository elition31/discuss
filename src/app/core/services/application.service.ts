// Angular
import { Injectable } from "@angular/core";

// Libraries
import { BehaviorSubject } from "rxjs";

// Models
import { Color } from "../models/color.interface";
import { Discuss } from "../models/discuss.interface";

@Injectable({
  providedIn: "root",
})
export class ApplicationService {
  /**
   * The color theme to apply
   */
  public currentColor: BehaviorSubject<Color | undefined> = new BehaviorSubject<Color | undefined>(undefined);

  /**
   * The page when the user is redirected when he clicks on the return button
   */
  public returnPath: BehaviorSubject<string> = new BehaviorSubject<string>("");

  /**
   * The current selected Discuss
   */
  public currentDiscuss: BehaviorSubject<Discuss | undefined> = new BehaviorSubject<Discuss | undefined>(undefined);

  /**
   * The current tab selected in the Discuss page
   */
  public discussSelectedTab: BehaviorSubject<"messages" | "members" | "settings" | "pinnedMessages"> = new BehaviorSubject<
    "messages" | "members" | "settings" | "pinnedMessages"
  >("messages");

  constructor() {}
}
