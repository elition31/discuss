// Angular
import { Injectable } from "@angular/core";

// Firebase
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import * as firebase from "firebase/firestore";

// Models
import { User } from "../models/user.interface";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  /**
   * Contains the data of the current authenticated user
   */
  private currentAuthenticateUser: User | undefined;

  private isRegistered: boolean = false;

  /**
   * This property define when the service designates if there is an authenticate user or not
   */
  public isAuthReady: boolean = false;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    // Check if the user is logged in
    this.afAuth.authState.subscribe((state) => {
      if (state) {
        // Get the data from Firestore Database
        this.afs
          .collection<User>("users")
          .doc(state.uid)
          .get()
          .subscribe(async (user) => {
            if (user.exists) {
              this.currentAuthenticateUser = user.data();
              this.isAuthReady = true;
            } else if (this.isRegistered) {
              try {
                await this.signOut();
              } catch (error) {
                console.log(error);
              } finally {
                this.isAuthReady = true;
              }
            }
          });
      } else {
        this.currentAuthenticateUser = undefined;
        this.isAuthReady = true;
      }
    });
  }

  /**
   * Returns the data of the authenticated user
   */
  public getCurrentUser(): User | undefined {
    return this.currentAuthenticateUser;
  }

  /**
   * This method is called to allow user to sign in
   * @param {string} email
   * @param {string} password
   */
  public signIn(email: string, password: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afAuth.signInWithEmailAndPassword(email, password);
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }

  public signUp(email: string, password: string, firstname: string, lastname: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await this.afAuth.createUserWithEmailAndPassword(email, password);
        await this.signIn(email, password);
        const date = firebase.Timestamp.now();
        await this.afs.collection<User>("users").doc(data.user?.uid).set({ uid: data.user?.uid!, firstname, lastname, createdAt: date });
        this.isRegistered = true;
        this.currentAuthenticateUser = { uid: data.user?.uid!, firstname, lastname, createdAt: date };
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * This method is called to allow user to sign out
   */
  public signOut(): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afAuth.signOut();
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }
}
