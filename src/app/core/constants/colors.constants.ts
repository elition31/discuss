import { Color } from "../models/color.interface";

export const COLORS: Color[] = [
  // Bleu-vert
  {
    id: 1,
    primary: "#10977c",
    secondary: "#1abc9c",
    shadow: "#035041",
    message: {
      other: "#10977c",
      owner: "#09725d",
      reply: "#e1e1e1",
    },
  },
  // Vert
  {
    id: 2,
    primary: "#27ae60",
    secondary: "#2ecc71",
    shadow: "#054d23",
    message: {
      other: "#27ae60",
      owner: "#178d48",
      reply: "#e1e1e1",
    },
  },
  // Bleu
  {
    id: 3,
    primary: "#2980b9",
    secondary: "#3498db",
    shadow: "#093857",
    message: {
      other: "#2980b9",
      owner: "#186ca3",
      reply: "#e1e1e1",
    },
  },
  // Violet
  {
    id: 4,
    primary: "#8e44ad",
    secondary: "#9b59b6",
    shadow: "#48195c",
    message: {
      other: "#8e44ad",
      owner: "#73328f",
      reply: "#e1e1e1",
    },
  },
  // Jaune
  {
    id: 5,
    primary: "#f39c12",
    secondary: "#f1c40f",
    shadow: "#a3690d",
    message: {
      other: "#f39c12",
      owner: "#d1760d",
      reply: "#e1e1e1",
    },
  },
  // Orange
  {
    id: 6,
    primary: "#d35400",
    secondary: "#e67e22",
    shadow: "#732e00",
    message: {
      other: "#d35400",
      owner: "#b94a00",
      reply: "#e1e1e1",
    },
  },
  // Rouge
  {
    id: 7,
    primary: "#d54435",
    secondary: "#e74c3c",
    shadow: "#751d14",
    message: {
      other: "#d54435",
      owner: "#c0392b",
      reply: "#e1e1e1",
    },
  },
  // Gris
  {
    id: 8,
    primary: "#7f8c8d",
    secondary: "#95a5a6",
    shadow: "#616a6b",
    message: {
      other: "#7f8c8d",
      owner: "#6f7878",
      reply: "#e1e1e1",
    },
  },
  // Rose
  {
    id: 9,
    primary: "#f558a4",
    secondary: "#fd79a8",
    shadow: "#961d58",
    message: {
      other: "#f558a4",
      owner: "#e84393",
      reply: "#e1e1e1",
    },
  },
];
